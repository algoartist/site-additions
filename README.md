# What is this?
Site Additions are the various additions to the Algo Artist Project

### Motivations
Creating pieces that capture experimental combinations of algorithms. 

It is my hope to showcase these in a gallery on algoartist.com and 
(eventually) in physical form, on a canvas.

### Where are the custom algorithms?
Please inquire about the authored algorithms I use to achieve the effects.


### Technology Stack
Using CI/CD, Python, Docker, Jenkins, Ansible, and the rest of the DevOps 
pipeline puppet pals :)